FROM registry.gitlab.com/clecherbauer/docker-images/python:3.8-debian-bullseye

COPY .build /.build
RUN /.build/build.sh

EXPOSE 80

CMD ["python3"]
