#!/bin/bash

set -e

function removeUnusedFiles() {
    find /usr/local -depth \
        \( \
            \( -type d -a \( -name test -o -name tests \) \) \
            -o \
            \( -type f -a \( -name '*.pyc' -o -name '*.pyo' \) \) \
    \) -exec rm -rf '{}' +
}

apt-get update
wget -q https://dl.google.com/linux/direct/google-chrome-stable_current_amd64.deb
apt-get install -y -f ./google-chrome-stable_current_amd64.deb
rm google-chrome-stable_current_amd64.deb

#cleanup
apt-get clean
rm -rf /var/lib/apt/lists/*
rm -rf /tmp/*
removeUnusedFiles

rm -rf /.build
